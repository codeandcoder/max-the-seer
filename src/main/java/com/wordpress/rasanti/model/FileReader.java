package com.wordpress.rasanti.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;

public class FileReader implements Reader {

	public Node readSession(String fileName) {
		Node root = new Node();
		try {
			File file = FileManager.getFileWithOS(fileName);
			
			Logger logger = Logger.getLogger("log");
			logger.info("Reading nodes data...");
			
			BufferedReader r = new BufferedReader(new java.io.FileReader(file));
			String line = r.readLine();

			if (line != null) {
				String info[] = line.split("\\|");
				root.setContent(info[0]);

				this.readNode(root, r, info[1], info[2]);
			} else {
				root.setContent("es un animal");
				root.makeSon("Perro", true);
				root.makeSon("Puerta", false);
			}

			r.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return root;
	}

	private String readNode(Node father, BufferedReader r, String positiveSon,
			String negativeSon) {
		
		Logger logger = Logger.getLogger("log");
		logger.info("Reading node son of [" + father.getContent() + "]...");
		
		String line = null;
		try {
			line = r.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (line != null) {
			String info[] = line.split("\\|");
			Node n = null;
			if (info[0].equals(positiveSon)) {
				n = father.makeSon(info[0], true);
				String other = this.readNode(n, r, info[1], info[2]);
				info = other.split("\\|");
			}
			if (info[0].equals(negativeSon)) {
				n = father.makeSon(info[0], false);
				String other = this.readNode(n, r, info[1], info[2]);
				info = other.split("\\|");
			}
			return info[0] + "|" + info[1] + "|" + info[2];
		}
		return "o|o|o";
	}

}
