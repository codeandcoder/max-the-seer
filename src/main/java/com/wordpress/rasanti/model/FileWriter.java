package com.wordpress.rasanti.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.log4j.Logger;

public class FileWriter implements Writer {

	public void writeSession(Node root, String fileName) {
		File file = FileManager.getFileWithOS(fileName);
		try {
			Logger logger = Logger.getLogger("log");
			logger.info("Writing nodes data...");
			PrintWriter writer = new PrintWriter(new BufferedWriter(new java.io.FileWriter(file)));
			writeNode(root, writer);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void writeNode(Node node, PrintWriter writer) {
		Logger logger = Logger.getLogger("log");
		logger.info("Writing node [" + node.getContent() + "]...");
		if (node != null) {
			writer.println(node.toString());

			if ( !node.isLeaf() ) {
				this.writeNode(node.getPositiveSon(), writer);
				this.writeNode(node.getNegativeSon(), writer);
			}
		}
	}

}
