package com.wordpress.rasanti.model;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

public class FileManager {
	
	// Can't be instantiated
	private FileManager() {}
	
	public static File getFileWithOS(String fileName) {
		String path = "";
		
		Logger logger = Logger.getLogger("log");
		logger.info("Getting storage file...");
		
		String os = System.getProperty("os.name").toLowerCase();
		if ( os.contains("win") ) {
			String username = System.getProperty("user.name");
			path = "C:/Users/" + username + "/AppData/Roaming";
		} else {
			path = System.getProperty("user.home");
		}
		
		path += "/.maxtheseer";
		
		File folder = new File(path);
		if ( !folder.exists() )
			folder.mkdir();
		
		path += "/" + fileName;
		File storageFile = new File(path);
		if ( !storageFile.exists() ) {
			try {
				storageFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return storageFile;
	}
	
}
