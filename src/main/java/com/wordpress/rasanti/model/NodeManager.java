package com.wordpress.rasanti.model;

public abstract interface NodeManager {

	public abstract Node readSession();
	public abstract void writeSession(Node root);

}
