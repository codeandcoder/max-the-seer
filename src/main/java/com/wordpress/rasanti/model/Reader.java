package com.wordpress.rasanti.model;

public interface Reader {
	
	public abstract Node readSession(String fileName);
	
}
