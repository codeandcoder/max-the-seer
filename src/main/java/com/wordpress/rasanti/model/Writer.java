package com.wordpress.rasanti.model;

public interface Writer {
	
	public abstract void writeSession(Node root, String fileName);

}
