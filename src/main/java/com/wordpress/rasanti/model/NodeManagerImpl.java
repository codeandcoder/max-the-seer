package com.wordpress.rasanti.model;

public class NodeManagerImpl implements NodeManager {
	
	public static final String STORAGE_FILE = "mts_nodes";
	protected Reader reader;
	protected Writer writer;

	public Node readSession() {
		Node root = reader.readSession(STORAGE_FILE);
		return root;
	}
	
	public void writeSession(Node root) {
		writer.writeSession(root, STORAGE_FILE);
	}
	
	// Getters & Setters
	public Reader getReader() {
		return reader;
	}

	public void setReader(Reader reader) {
		this.reader = reader;
	}

	public Writer getWriter() {
		return writer;
	}

	public void setWriter(Writer writer) {
		this.writer = writer;
	}

}
