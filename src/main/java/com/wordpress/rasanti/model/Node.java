package com.wordpress.rasanti.model;

import org.apache.log4j.Logger;

import com.wordpress.rasanti.controller.ApplicationMain;


public class Node {

	private Node positiveSon;
	private Node negativeSon;
	private String content;
	
	public Node makeSon (String content, boolean side) {	
		Logger logger = Logger.getLogger("log");
		logger.info("Making new son... [" + content + "]" );
		
		if (side) {
			positiveSon = new Node();
			positiveSon.setContent(content);
			return positiveSon;
		}
		// else
		negativeSon = new Node();
		negativeSon.setContent(content);
		return negativeSon;
	}
	
	public void convertToQuestion (String question, boolean sonAnswer) {
		this.makeSon(this.content, sonAnswer);
		setContent(question);
	}
	
	public boolean isLeaf() {
		return positiveSon == null && negativeSon == null;
	}
	
	// Getters && Setters
	public Node getPositiveSon() {
		return positiveSon;
	}
	
	public void setPositiveSon(Node positiveSon) {
		this.positiveSon = positiveSon;
	}
	
	public Node getNegativeSon() {
		return negativeSon;
	}
	
	public void setNegativeSon(Node negativeSon) {
		this.negativeSon = negativeSon;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content.trim();
	}
	
	@Override
	public String toString() {
		String positiveSon = this.positiveSon == null ? "null" : this.positiveSon.getContent();
		String negativeSon = this.negativeSon == null ? "null" : this.negativeSon.getContent();
		
		return this.content + "|" + positiveSon + "|" + negativeSon;
	}
	
}
