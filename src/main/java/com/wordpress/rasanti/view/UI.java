package com.wordpress.rasanti.view;

import com.wordpress.rasanti.controller.Controller;

public interface UI {
	
	public abstract void init(Controller controller);

}
