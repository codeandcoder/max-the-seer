package com.wordpress.rasanti.view;

import java.util.Scanner;

import org.apache.log4j.Logger;

import com.wordpress.rasanti.controller.Controller;
import com.wordpress.rasanti.model.Node;

public class SimpleTUI implements UI {

	public static final String PROMPT = ">> ";
	private Controller controller;
	private Node root;
	
	public void init(Controller controller) {
		Logger logger = Logger.getLogger("log");
		logger.info("Initializing view...");
		this.controller = controller;
		
		root = controller.readSession();
		this.lookForAnswer ( root );
	}
	
	private void lookForAnswer (Node node) {
		boolean finish = false;
		
		boolean positiveAnswer = requestAnswer(node.getContent());
		Node n = positiveAnswer ? node.getPositiveSon() : node.getNegativeSon();
		while (n != null && !finish) {
			if (n.isLeaf()) {
				boolean isWinner = isThing(n.getContent());
				if ( isWinner ) {
					finish = true;
					winner();
				} else {
					String newSonContent = requestThing();
					String newContent = requestQuestion(n.getContent());
					n.convertToQuestion(newContent, false);
					n.makeSon(newSonContent, true);
					finish = true;
				}
			} else {
				boolean ans = requestAnswer(n.getContent());
				n = ans ? n.getPositiveSon() : n.getNegativeSon();
			}
		}
		
		controller.writeSession(root);
	}
	
	private boolean requestAnswer(String question) {
		Scanner sc = new Scanner (System.in);
		System.out.println("�Lo que est�s pensando " + question + "?");
		System.out.print(PROMPT);
		String ans = sc.next();
		ans = ans.toLowerCase();
		return ans.equals("s");
	}
	
	private boolean isThing(String thing) {
		Scanner sc = new Scanner (System.in);
		System.out.println("Estas pensando en " + thing + "?");
		System.out.print(PROMPT);
		String ans = sc.next();
		ans = ans.toLowerCase();
		return ans.equals("s");
	}
	
	private void winner() {
		System.out.println("Soy un crack");
	}

	private String requestThing() {
		Scanner sc = new Scanner (System.in);
		System.out.println("En qu� estabas pensando?");
		System.out.print(PROMPT);
		String ans = sc.next();
		return ans;
	}

	private String requestQuestion(String previous) {
		Scanner sc = new Scanner (System.in).useDelimiter("\n");
		System.out.println("Qu� tiene de diferente con " + previous + " ?");
		System.out.print(PROMPT);
		String ans = sc.next();
		return ans;
	}

}
