package com.wordpress.rasanti.controller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationMain {
	
	public static ApplicationContext context;
	public static Controller controller;

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("/spring-conf.xml");

		controller = context.getBean(Controller.class);
		controller.initView();
	}
	
}
