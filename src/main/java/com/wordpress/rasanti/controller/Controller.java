package com.wordpress.rasanti.controller;

import com.wordpress.rasanti.model.Node;
import com.wordpress.rasanti.model.NodeManager;
import com.wordpress.rasanti.view.UI;

public class Controller {

	protected UI userInterface;
	protected NodeManager nodeManager;

	public void initView() {
		userInterface.init(this);
	}
	
	public void writeSession(Node root) {		
		nodeManager.writeSession(root);
	}
	
	public Node readSession() {
		return nodeManager.readSession();
	}
	
	// Getters && Setters
	public NodeManager getNodeManager() {
		return nodeManager;
	}

	public void setNodeManager(NodeManager nodeManager) {
		this.nodeManager = nodeManager;
	}

	public UI getUserInterface() {
		return userInterface;
	}

	public void setUserInterface(UI userInterface) {
		this.userInterface = userInterface;
	}
	
	
}
